<?php

class Document extends Eloquent {

	public static function upload_file($key,$file,$filename = null,$overwrite = false)
	{
		$extension = File::extension($file['name']);
		$directory = path('public').'uploads/documents/';
		if(!is_dir($directory))
		{
			$mkdir = mkdir($directory);
		}

		$filename = $filename ? $filename : $file['name'];

		if ($overwrite)
		{
			$file = $directory.basename($file['name']);
			if (file_exists($file))
        		unlink($file);
		}
		
		$upload_success = Input::upload($key, $directory, strtolower($filename));

		if ($upload_success)
		{
			return array(
				'path'	=> $directory . $filename,
				'extension' => static::get_file_property($directory.$filename,'extension'),
				'name'	=> static::get_file_property($directory.$filename,'filename')
			);
		}
		else
		{
			return null;
		}
	}

	private static function get_file_property($filename,$propery)
	{
		$parts = pathinfo($filename);
		return strtolower($parts[$propery]);
	}
}
