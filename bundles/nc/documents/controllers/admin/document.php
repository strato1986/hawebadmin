<?php

class Documents_Admin_Document_Controller extends Base_Controller {

	public $restful = true;

	private $rules = array(
		'document_title'		=> 'required',
		'document_description'	=>	'required',
		'document_file'			=>	'mimes:pdf,doc,docx',
	);

	public function get_index()
	{
		$documents = Document::order_by('created_at', 'desc')->paginate(5);

		return View::make('documents::admin.document.index')
			->with('documents',$documents);
	}

	public function get_new()
	{
		$form = Formly::make();
		$form->display_inline_errors = true;
		$form->form_class = 'form_vertical';

		$data = array(
			'form'	=> $form,
			'categories_list'	=> Category::get_select_items(),
		);

		return View::make('documents::admin.document.form',$data);
	}

	public function post_new()
	{
		$v = Validator::make(Input::all(), $this->rules);

		if ($v->fails())
		{
			return Redirect::to('admin/document/new')
				->with_errors($v)
				->with_input();
		}

		$document = Document::upload_file('document_file',Input::file('document_file'),null,true);
		if ($document)
		{
			$directory = path('public').'uploads/';
			$filename = $document['name'].'.'.$document['extension'];

			Document::create(array(
				'document_title'	=>	Input::get('document_title'),
				'category_id'		=>	Input::get('category_id'),
				'document_description'	=>	Input::get('document_description'),
				'url'				=>	asset('uploads/documents/' . $filename),
				'path'				=>	$directory . $filename,
			));
		}
		return Redirect::to('admin/document')->with('success_message','Documento creado correctamente');

	}
}
