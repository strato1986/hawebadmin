<?php

class Documents_Create_Documents {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('documents',function($table)
		{
			$table->increments('id');
			$table->string('document_title');
			$table->integer('category_id');
			$table->text('document_description');
			$table->string('url');
			$table->string('path');
			$table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('documents');
	}

}