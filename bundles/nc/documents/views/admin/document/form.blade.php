@layout('admin::templates.main')
@section('content')
	<h2>
    @if(Request::route()->controller_action == 'new')
        Nuevo 
    @else
        Editar
    @endif
        documento
    </h2>
    <hr />

	{{ $form->open_for_files() }}
		{{ Form::token() }}
		<div class="row">
			<div class="span4">
				{{ $form->text('document_title','Titulo') }}
			</div>
			<div class="span4">
				{{ $form->select('category_id','Categoria', $categories_list, isset($post->category_id) ? $post->category_id : 4 ) }}
				</div>
		</div>
		{{ $form->textarea('document_description', 'Descripcion',null,array('id' => 'ckeditor'))}}
		{{ $form->file('document_file','Documento') }}
		{{ $form->submit('Guardar') }}
		
	{{ $form->close() }}

@endsection


@section('assets')
    <script src="{{ URL::to_asset('js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ URL::to_asset('js/ckeditor/config.js') }}"></script>
    <script src="{{ URL::to_asset('js/ckeditor/jquery.js') }}"></script>

    <script type="text/javascript">
        $(function()
        {
            var config = {
                enterMode : CKEDITOR.ENTER_P,
                shiftEnterMode : CKEDITOR.ENTER_P
            };

            $('#ckeditor').ckeditor(config);
        });
    </script>
@endsection