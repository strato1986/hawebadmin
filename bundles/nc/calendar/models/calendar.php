<?php

class Calendar extends Eloquent {

	public function set_due_date($date)
	{
		$due_date = explode(' ', $date);
		
		$fecha = explode('/',$due_date[0]);
		//$fechasql = $fecha[2].'-'.$fecha[1].'-'.$fecha[0].' '.$due_date[1].':00';
		$fechasql = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];

		//die( date($date) );
		$this->set_attribute('due_date',date($fechasql));

	}

	public function get_due_date()
	{
		$due_date = $this->get_attribute('due_date');
		return date('d/m/Y h:i', strtotime($due_date));
	}

	public function short_date()
	{
		$due_date = $this->get_attribute('due_date');
		return date('d/m',strtotime($due_date));
	}

	public function compare_date_with_now()
	{
		$today = strtotime(date('Y-m-d'));
		$result = array(
			'css_class'	=> null
		);

		$due_date = strtotime($this->get_attribute('due_date'));
		//dd($calendar->get_attribute('due_date'));

		if ($today > $due_date)
			$result['css_class'] = null;
		elseif ($due_date == $today)
			$result['css_class'] = 'red';
		else
		{	
			$day_before = strtotime('-1 day',$due_date);
			$week_before = strtotime('-1 week',$due_date);

			if ($today == $day_before)
				//echo "Falta un dia para el vencimiento".PHP_EOL;
				$result['css_class'] = 'orange';
			elseif ($today == $week_before)
				//echo "Falta una semana para el vencimiento".PHP_EOL;
				$result['css_class'] = 'yellow';
			elseif ($today > $week_before)
				//echo "Falta menos de una semana para el vencimiento".PHP_EOL;
				$result['css_class'] = 'yellow';
			else
				//echo "Falta mas de una semana para el vencimiento".PHP_EOL;
				$result['css_class'] = 'green';
		}

		return $result;
	}
}
