<?php

class Calendar_Create_Calendars {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('calendars',function($table)
		{
			$table->increments('id');
			$table->string('event_title');
			$table->date('due_date');
			$table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('calendars');
	}

}