<?php

class Calendar_Add_Monthly {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('calendars',function($table)
		{
			$table->boolean('monthly')->nullable();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('calendars',function($table)
		{
			$table->drop_column('monthly');
		});
	}

}