@layout('admin::templates.main')
@section('content')
	<h2>
    @if(Request::route()->controller_action == 'new')
        Nueva 
    @else
        Editar
    @endif
        fecha de vencimiento
    </h2>
    <hr />
	{{ $form->open() }}
		{{ Form::token() }}
		@if(Request::route()->controller_action == 'edit')
			{{ Form::hidden('id', $calendar->id) }}
		@endif
		{{ $form->text('event_title', 'Titulo', isset($calendar) ? $calendar->event_title : null) }}
		{{ $form->text('due_date', 'Fecha de vencimiento', isset($calendar) ? $calendar->due_date : null,array('class' => 'datepicker')) }}
		{{ $form->checkbox('monthly','Mensualmente',1,isset($calendar) ? $calendar->monthly : null) }}
		{{ $form->submit('Guardar') }}
	{{ $form->close() }}
@endsection

@section('assets')

	<link href="{{ URL::to_asset('css/kendoui/kendo.common.min.css')}}" rel="stylesheet" />
    <link href="{{ URL::to_asset('css/kendoui/kendo.metro.min.css')}}" rel="stylesheet" />
	<script src="{{ URL::to_asset('js/kendoui/kendo.web.min.js') }}"></script>
	<script src="{{ URL::to_asset('js/kendoui/kendo.datetimepicker.min.js') }}"></script>
	<script src="{{ URL::to_asset('js/kendoui/kendo.culture.es-AR.min.js') }}"></script>
    <script type="text/javascript">
        $(function()
        {
        	kendo.culture('es-AR');
        	$(".datepicker").kendoDatePicker();      
        });
    </script>
@endsection