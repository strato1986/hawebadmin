@layout('admin::templates.main')
@section('content')
 <h1>Calendario</h1>

  <a href="{{ URL::to('admin/calendar/new') }}">Nueva fecha</a>
	  @if (Session::has('success_message'))
    <div class="span8">
      {{ Alert::success(Session::get('success_message')) }}
    </div>
  @endif


    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Titulo</th>
                <th>Fecha limite</th>
            </tr>
        </thead>
        <tbody>
    @foreach ($calendars->results as $calendar)
        <tr>
            <td>{{ $calendar->event_title }}</td>
            <td><span class="badge badge-success">{{$calendar->due_date}}</span></td>
            <td>
                {{ HTML::link('admin/calendar/edit/'.$calendar->id, 'Editar', array('class' => 'btn btn-success')) }}
                <a class="delete_toggler btn btn-danger" rel="{{$calendar->id}}">Eliminar</a>
            </td>
		</tr>
        
    @endforeach
        <tbody>
    </table>

    <div class="modal hide fade" id="delete_modal">
      <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <h3>Esta seguro?</h3>
      </div>
      <div class="modal-body">
        <p>Esta seguro de que quiere eliminar esta fecha?</p>
      </div>
      <div class="modal-footer">
        {{ Form::open('admin/calendar/delete', 'POST') }}
        <a data-toggle="modal" href="#delete_modal" class="btn">No</a>
        <input type="hidden" name="id" id="postvalue" value="" />
        <input type="submit" class="btn btn-danger" value="Eliminar" />
        {{ Form::close() }}
      </div>
    </div>

    <script>
      $('#delete_modal').modal({
        show:false
      }); // Start the modal

      // Populate the field with the right data for the modal when clicked
      $('.delete_toggler').each(function(index,elem) {
          $(elem).click(function(){
            $('#postvalue').attr('value',$(elem).attr('rel'));
            $('#delete_modal').modal('show');
          });
      });
    </script>
@endsection