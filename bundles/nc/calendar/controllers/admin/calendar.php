<?php

class Calendar_Admin_Calendar_Controller extends Base_Controller {

	public $restful = true;

	private $rules = array(
		'event_title'	=>	'required',
		'due_date'		=>	'required'
	);

	public function get_index()
	{
		$calendars = Calendar::order_by('updated_at', 'desc')->paginate(5);
		return View::make('calendar::admin.calendar.index')->with('calendars',$calendars);
	}

	public function get_new()
	{
		$form = Formly::make();
		$form->display_inline_errors = true;
		
		$data = array(
			'form'	=> $form,
		);

		return View::make('calendar::admin.calendar.form',$data);
	}

	public function get_edit($id = null)
	{
		if (!$id) return Redirect::to('admin');

		$calendar = Calendar::find($id);
		if (!$calendar)
			return Redirect::to('admin/calendar')->with('error_message','La fecha que esta buscando no existe.');

		$form = Formly::make();
		$form->display_inline_errors = true;
		$form->form_class = 'form_vertical';
		$data = array(
			'form'	=> $form,
			'calendar'	=> $calendar
		);

		return View::make('calendar::admin.calendar.form',$data);
	}
	public function post_new()
	{
		date_default_timezone_set('America/Argentina/Ushuaia');
		$new_calendar = array(
			'event_title'	=> 	Input::get('event_title'),
			'due_date'	=>	Input::get('due_date'),
			'monthly'	=> Input::get('monthly')
		);

		$v = Validator::make($new_calendar, $this->rules);
		if ( $v->fails() )
		{
			return Redirect::to('admin/calendar/new')
				->with_errors($v)
				->with_input();
		} 

		
		$calendar = new Calendar($new_calendar);
		$calendar->save();
		
		return Redirect::to('admin/calendar')->with('success_message','Datos guardados correctamente');		
	}

	public function post_edit()
	{
		$v = Validator::make(Input::all(), $this->rules);
		$id = Input::get('id');
		
		if ($v->fails())
		{
			return Redirect::to('admin/calendar/edit/'.$id)
				->with_errors($v)
				->with_input();
		}

		$calendar = Calendar::find($id);
		$calendar->event_title = Input::get('event_title');
		$calendar->due_date	= Input::get('due_date');
		$calendar->monthly = Input::get('monthly');
		$calendar->save();

		return Redirect::to('admin/calendar');
	}
	public function post_delete($id = null)
	{
		$rules = array(
            'id'  => 'required|exists:calendars',
        );
        $v = Validator::make(Input::all(), $rules);

        if ($v->fails())
        {
            return Redirect::to('admin/calendar')->with('error_message','No se pudo eliminar la fecha');
        }else{
            //Uploadr::remove('page',Input::get('id'));
            $calendar = Calendar::find(Input::get('id'));
            $calendar->delete();
            //Messages::add('success_message','Page Removed');
            return Redirect::to('admin/calendar')->with('success_message', 'Fecha eliminada correctamente');
        }

	}

}
