<?php

class Setting_Create_Settings {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('settings',function($table)
		{
			$table->increments('id');
			$table->string('key_setting');
			$table->string('value_setting');
			$table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('settings');
	}

}