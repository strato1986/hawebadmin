@layout('admin::templates.main')
@section('content')
	<!-- check for login errors flash var -->
	@if (Session::has('success_message'))
		{{ Alert::success(Session::get('success_message')) }}
	@endif
	{{ $form->open_for_files() }}
		{{ $form->file('logo','Logo')}}
		@if(isset($load_settings['logo_url']))
			<img src="{{$load_settings['logo_url']}}" title="Logo" style='height:150px'/>
		@endif
		{{ $form->text('site_title','Titulo', isset($load_settings['site_title']) ? $load_settings['site_title'] : null )}}
		{{ $form->submit('Guardar') }}
	{{ $form->close() }}
@endsection