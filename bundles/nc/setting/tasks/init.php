<?php

class Setting_Init_Task extends Task {

	public function run($arguments)
	{
		$site_title = new Setting(array(
			'key_setting'	=>	'site_title',
			'value_setting'	=>	'Sitio Nuevo'
		));

		$site_title->save();
		
		echo "Cargadas preferencias iniciales".PHP_EOL;
	}

}
