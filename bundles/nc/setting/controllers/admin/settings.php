<?php

class Setting_Admin_Settings_Controller extends Base_Controller {

	public $restful = true;

	public function get_index()
	{
		$settings = Setting::all();

		$load_settings = array();
		foreach($settings as $setting)
		{
			$load_settings[$setting->key_setting] = $setting->value_setting;
		}

		unset($settings);

		$form = Formly::make();
		$form->show_inline_errors = true;

		$data = array(
			'form'	=> $form,
			'load_settings'	=> $load_settings,
		);

		return View::make('setting::admin.settings.index',$data);
	}

	public function post_index()
	{
		$settings = Input::all();
		if(Input::has_file('logo')) 
		{
			$result = Setting::set_logo(Input::file('logo'));
		}
		
		if (Input::has('site_title'))
		{
			$result = Setting::set('site_title',Input::get('site_title'));
		}



		if ($result)
			return Redirect::to('admin/settings')->with('success_message','Datos guardados correctamente');
	}
}
