<?php

class Setting extends Eloquent {

	public static function get($value='')
	{
		$setting = Setting::where('key_setting', '=', $value)->first()->value_setting;
		return $setting;
	}

	public static function get_logo()
	{
		$logo = Setting::where('key_setting','=', 'logo_url')->first();
		if ($logo)
			return $logo->value_setting;
		return '';
	}
	public static function set_logo($file='')
	{
		$user = Sentry::user();
		if (!file_exists(path('public').'uploads/'.sha1($user->id)))
				mkdir(path('public').'uploads/'.sha1($user->id));

		$upload_file = Multimedia::upload_file('logo',$file,'logo.png');

		if( $upload_file ) {
        	$logo = Setting::where('key_setting','=','logo_url')->first();
        	if ($logo)
        	{
        		$logo->value_setting = asset('uploads/'.$upload_file['name'].'.'.$upload_file['extension']);
        	}
        	else
        	{
        		$logo = new Setting(array(
            	'key_setting'	=>	'logo_url',
                'value_setting' => asset('uploads/'.$upload_file['name'].'.'.$upload_file['extension']),
            	));
        	}
            
            $logo->save();

            return true;
        }

        return false;
	}

	public static function set($key='',$value='')
	{
		$setting = Setting::where('key_setting','=',$key)->first();

		if (!$setting)
		{
			$setting = new Setting();
			$setting->key_setting = $key;
		}

		$setting->value_setting = $value;
		$setting->save();

		return true;
	}
}
