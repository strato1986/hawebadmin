<?php

class User_User_Controller extends Base_Controller {

	public $restful = true;

	public function get_index()
	{
		// code here..

		return View::make('user::user.index');
	}

	public function get_logout()
	{
		Sentry::logout();
		return Redirect::to('/');
	}

	public function get_login()
	{
		$form = Formly::make();
		$form->form_class = "form-inline";
		$form->display_inline_errors = true;
		return View::make('user::user.login')->with('form', $form);
	}

	public function post_login()
	{
		$post_login = array(
			'username'	=> Input::get('username'),
			'password'	=> Input::get('password')
		);
		
		$rules = array(
			'username'	=> 'required',
			'password'	=> 'required'
		);

		$v = Validator::make($post_login,$rules);
		if ($v->fails())
		{
			return Redirect::to('user/login')->with_errors($v);
		}
		else
		{
			try {
				$login = Sentry::login($post_login['username'],$post_login['password'],false);
				if ($login)
				{
					return Redirect::to('/admin');
				}
				
			} catch (Sentry\SentryException $e) {
				$errors = $e->getMessage();
				return Redirect::to('/user/login')->with('login_errors',$errors);
			}
		}
		
	}
}
