<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Bootstrap, from Twitter</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	
	
	<style type="text/css">
		body { padding-top: 60px; padding-bottom: 40px; }
	</style>
	

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="http://twitter.github.com/bootstrap/assets/images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="http://twitter.github.com/bootstrap/assets/images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="http://twitter.github.com/bootstrap/assets/images/apple-touch-icon-114x114.png">

	{{ Asset::container('bootstrapper')->styles() }}
	{{ Asset::container('bootstrapper')->scripts() }}
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="span5 offset4 well">
			{{ $form->open() }}

				<!-- check for login errors flash var -->
				@if (Session::has('login_errors'))
					{{ Alert::error(Session::get('login_errors')) }}
				@endif

				<!-- username field -->
				{{ $form->text('username','Usuario') }}

				<!-- password field -->
				{{ $form->password('password','Contraseña') }}

				<!-- submit button -->
				<p>{{ $form->submit('Login', array('class' => 'btn-large')) }}</p>

			{{ $form->close() }}
			</div>
		</div>
	</div> <!-- /container -->

</body>
</html>

