<?php

class User_Create_Admin_User {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Bundle::start('sentry');
		Session::load();
		$users = DB::table('users')->count();
		if ($users == 0)
		{
			try {

		    	$user_id = Sentry::user()->create(array(
				    'email' => 'nahuel.chaves@gmail.com',
				    'username'	=> 'admin',
				    'password' => 'Strato1986',
				    'metadata' => array(
					    'first_name' => 'Admin',
					    'last_name' => 'User',
					    // add any other fields you want in your metadata here. ( must add to db table first )
					    ),
				    //'permissions' => array('is_admin' => true)
			    ));

		    if ($user_id)
		    {
		    	
		    	Sentry::user($user_id)->update_permissions(array(
		    		'is_admin'	=> 1
		    		
		    	));
				
		    	echo "Ran task: User_Create_Admin_User_Task : run()".PHP_EOL;
		    }
		    else
		    {
		    	// something went wrong - shouldn't really happen
		    }
		    }
		    catch (Sentry\SentryException $e)
		    {
		    	echo $e->getMessage(); // catch errors such as user exists or bad fields

		    }
		}
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}