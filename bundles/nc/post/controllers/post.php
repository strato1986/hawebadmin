<?php

class Post_Post_Controller extends Frontend_Controller {

	public $restful = true;

	public function get_index($post_id='')
	{
		if (!isset($post_id))
			return Redirect::to('/');
		$post = Post::with('featuredimage')->find($post_id);
		
		if (!$post)
			return Redirect::to('/');
		return View::make('post::post.index')->with('post', $post);
	}


}
