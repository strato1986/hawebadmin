<?php


class Post_Admin_Post_Controller extends Backend_Controller {

	public $restful = true;


	private $rules = array(
			'post_title'	=> 'required|min:3|max:255',
			'post_body'		=> 'required|min:10'
		);


	public function get_index()
	{
		$posts = Post::order_by('updated_at', 'desc')->paginate(5);

		return View::make('post::admin.post.index')->with('posts', $posts);
	}

	public function get_new()
	{
		$form = Formly::make();
		$form->display_inline_errors = true;
		$form->form_class = 'form_vertical';
		$this->data = array(
			'user' => Sentry::user(),
			'form'	=> $form,
			'create' => true,
			'categories_list'	=> Category::get_select_items()
		);
		return View::make('post::admin.post.form', $this->data);
	}

	public function post_new()
	{
		$this->filter('before', 'csrf')->on('post');
		$upload_success = false;
		$new_post = array(
			'post_title'	=> Input::get('post_title'),
			'post_body'		=> Input::get('post_body'),
			'post_author'	=> Input::get('post_author'),
			'category_id'	=> Input::get('category')
		);

		$validation = Validator::make($new_post, $this->rules);

		if ( $validation->fails() )
		{
			return Redirect::to('admin/post/new')
				->with('user', Sentry::user())
				->with_errors($validation)
				->with_input();
		}

		$post = new Post($new_post);
		$post->save();
		

		$featured_image = Input::file('featured_image');

		if (Input::has_file('featured_image') and ($featured_image['size'] > 0 ))
		{
			
			$user = Sentry::user();
			
			
			$upload = Multimedia::upload_file('featured_image',$featured_image,null,true);

			if ($upload)
			{
				$directory = path('public').'uploads/';
				$filename = $upload['name'].'.'.$upload['extension'];

				$multimedia = new Multimedia();
				$multimedia->url = asset('uploads/' . $filename);
				$multimedia->path = $directory . $filename;
				$multimedia->save();


				$version50x50 = new Version();
				$version50x50->make($upload,array(50,50));
				$version50x50->save();
			    
			    $multimedia->versions()->insert($version50x50);

			    $version730x365 = new Version();
				$version730x365->make($upload,array(730,365));
				$version730x365->save();
			    
			    $multimedia->versions()->insert($version730x365);

			    $post->featuredimage()->insert($multimedia);
			}
		
		}

		return Redirect::to('admin/post')->with('success_message','Datos guardados correctamente');
	}
	

	public function post_edit()
	{
		$v = Validator::make(Input::all(), $this->rules);
		$id = Input::get('id');
		if ($v->fails())
		{
			return Redirect::to('admin/post/edit/' . $id)
				//->with('user', Sentry::user())
				->with_errors($v)
				->with_input();
		} else {
			$post = Post::find($id);
			$post->category_id = Input::get('category');
			$post->post_title = Input::get('post_title');
			$post->post_body = Input::get('post_body');

			$featured_image = Input::file('featured_image');

			if (Input::has_file('featured_image') and ($featured_image['size'] > 0 ))
			{
					$user = Sentry::user();
					
					

					$upload = Multimedia::upload_file('featured_image',$featured_image,null,true);

					if ($upload)
					{

						$directory = path('public').'uploads/';
						$filename = $upload['name'].'.'.$upload['extension'];

						$multimedia = new Multimedia();
						$multimedia->url = asset('uploads/' . $filename);
						$multimedia->path = $directory . $filename;
						$multimedia->save();

						$version50x50 = new Version();
						$version50x50->make($upload,array(50,50));
						$version50x50->save();

						$multimedia->versions()->insert($version50x50);

					    $version730x365 = new Version();
						$version730x365->make($upload,array(730,365));
						$version730x365->save();

						$multimedia->versions()->insert($version730x365);

						$post->featuredimage()->insert($multimedia);
						
						
					}
			}



			
			$post->save();

			return Redirect::to('admin/post');



		}
	}

	public function get_edit($id = null)
	{
		if (!$id) return Redirect::to('admin');

		$post = Post::with('featuredimage')->find($id);
		if (!$post)
			return Redirect::to('admin/post')->with('error_message','El post que esta buscando no existe.');

		$form = Formly::make();
		$form->display_inline_errors = true;
		$form->form_class = 'form_vertical';

		$this->data = array(
			'user' => Sentry::user(),
			'form' => $form,
			'post' => $post,
			'categories_list'	=> Category::get_select_items(),
			'featuredimage'	=>	$post->featuredimage
		);

		return View::make('post::admin.post.form',$this->data);
	}


	public function post_delete($id = null)
	{
		$rules = array(
            'id'  => 'required|exists:posts',
        );
        $validation = Validator::make(Input::all(), $rules);

        if ($validation->fails())
        {
            //Messages::add('error','You tried to delete a page that doesn\'t exist.');
            return Redirect::to('admin/post');
        }else{
            //Uploadr::remove('page',Input::get('id'));
            $post = Post::with('user')->find(Input::get('id'));
            $post->delete();
            //Messages::add('success_message','Page Removed');
            return Redirect::to('admin/post')->with('success_message', 'Post eliminado correctamente');
        }

	}

}
