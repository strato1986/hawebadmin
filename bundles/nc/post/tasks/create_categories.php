<?php

class Post_Create_Categories_Task extends Task {

	public function run($arguments)
	{
		Category::create(array(
			'name'	=> 'Noticias'
		));

		Category::create(array(
			'name'	=> 'Eventos'
		));		

		Category::create(array(
			'name'	=> 'Agenda'
		));

		Category::create(array(
			'name'	=> 'Transparencia'
		));

		Category::create(array(
			'name'	=> 'Guia de Tramites'
		));
		
		echo "Post :: Categorias Creadas".PHP_EOL;
	}

}
