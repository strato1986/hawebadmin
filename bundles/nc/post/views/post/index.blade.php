@layout('templates.frontend')
@section('content')

	<!-- BEGIN .post -->
		<div class="post-100 post type-post status-publish format-image hentry category-cities category-travel tag-italy odd" id="post-100">

		<!-- BEGIN .entry-header -->
		<div class="entry-header">
		
					
			<h1><span class="post-icon"></span>{{ $post->post_title }}</h1>
			
								
		</div>
		<!-- BEGIN .entry-header -->
		
		<!-- BEGIN .entry-meta -->
		<div class="entry-meta">
		
			<p>Por {{ $post->user->username }} en <a href="#" title="{{ $post->category->name }}" rel="">{{ $post->category->name }}</a> · {{ strftime(' %a %d, %Y', strtotime($post->created_at)) }} · <!--<a href="http://www.awesem.com/deadline-responsive/2012/05/28/schenna/#respond" rel="nofollow" title="Comment on Schenna">Sin comentarios</a>-->
			</p>
			
		</div>
		<!-- END .entry-meta -->
			
				
		<!-- BEGIN .entry-thumb -->
		<div class="entry-thumb">
					
			<img src="{{ $post->featuredimage->url }}" class="attachment-single-project-thumbnail wp-post-image" alt="schenna">		
		</div>
		<!-- END .entry-thumb -->
			
				
		<!-- BEGIN .entry -->
		<div class="entry">
						
			{{ $post->post_body }}
		
		</div>
		<!-- END .entry -->
			
	</div>
	<!-- END .post -->
@endsection