@layout('admin::templates.main')
@section('content')
<div class="span8">
    <h2>
    @if(Request::route()->controller_action == 'new')
        Nueva 
    @else
        Editar
    @endif
        publicacion
    </h2>
    <hr />
    {{ $form->open_for_files() }}
    	{{ Form::hidden('post_author', $user->id) }}
        @if(!isset($create))
            {{ Form::hidden('id', $post->id) }}
        @endif
        <div class="row">
            <div class="span4">
                {{ $form->text('post_title', 'Titulo', isset($post->post_title) ? $post->post_title : '') }}
            </div>
            <div class="span4">
                {{ $form->select('category','Categoria', $categories_list, isset($post->category_id) ? $post->category_id : null) }}
            </div>
        </div>
 
        {{ $form->textarea('post_body', 'Contenido', isset($post->post_body) ? $post->post_body : '', array('id' => 'ckeditor'))}}


        <div class="row">
            <div class="span4">
                {{ $form->file('featured_image', 'Imagen Principal') }}
            </div>
            <div class="span4">
                @if(isset($featuredimage))
                    <div class="featured_image">
                        <img src="{{ $featuredimage->url }}" title="imagen" data-featured-image="{{ $featuredimage->id }}" style="height:150px"/>
                        {{ Buttons::danger_normal('Eliminar',array('id' => 'delete_toggler', 'data-featured-image' => $featuredimage->id)) }}
                    </div>
                @endif
            </div>
        </div>
        <!-- submit button -->
        <p>{{ Form::submit('Guardar') }}</p>
    {{ $form->close() }}
</div>

    @if(isset($featuredimage))
        <div class="modal hide fade" id="delete_featuredimage">
              <div class="modal-header">
                <a class="close" data-dismiss="modal">×</a>
                <h3>Are You Sure?</h3>
              </div>
              <div class="modal-body">
                <p>Esta seguro de eliminar esta imagen?</p>
              </div>
              <div class="modal-footer">
                {{ Form::open('admin/multimedia/delete', 'POST', array('id' => 'form_delete_featuredimage')) }}
                    {{ Form::token() }}
                    <a data-toggle="modal" href="#delete_featuredimage" class="btn">No</a>
                    <input type="hidden" name="id" id="postvalue" value="{{ $featuredimage->id }}" />
                    <input type="submit" class="btn btn-danger" value="Eliminar" />
                {{ Form::close() }}
              </div>
            </div>

            <script>
              $('#delete_featuredimage').modal({
                show:false
              }); // Start the modal

              // Populate the field with the right data for the modal when clicked
              $('#delete_toggler').click(function(){
                    //$('#postvalue').attr('value',$(elem).attr('rel'));
                    $('#delete_featuredimage').modal('show');
             });
              
            $(function() 
            {   
                // bind all submit events (in case you have multiple forms on the page)
                $('#form_delete_featuredimage').bind('submit', function(e){

                    
                    e.preventDefault();
                    // get the form element
                    var form = $(this);
                            
                    // submit form
                    $(form).ajaxSubmit({    
                        // r = response; s = status             
                        dataType:  'json',  
                        success: function(r, s)             
                        {
                            if (r.success == "true")
                            {
                                $('#delete_featuredimage').modal('hide');
                                $('.featured_image').hide();
                            }       
                        }
                    })
                })
            })
            </script>
    @endif
@endsection


@section('assets')
    <script src="{{ URL::to_asset('js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ URL::to_asset('js/ckeditor/config.js') }}"></script>
    <script src="{{ URL::to_asset('js/ckeditor/jquery.js') }}"></script>

    <script type="text/javascript">
        $(function()
        {
            var config = {
                enterMode : CKEDITOR.ENTER_P,
                shiftEnterMode : CKEDITOR.ENTER_P
            };

            $('#ckeditor').ckeditor(config);
        });
    </script>
@endsection