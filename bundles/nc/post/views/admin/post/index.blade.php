@layout('admin::templates.main')
@section('content')	
  <h1>Blog</h1>

  <a href="{{ URL::to('admin/post/new') }}">Nueva entrada</a>
  @if (Session::has('success_message'))
    <div class="span8">
      {{ Alert::success(Session::get('success_message')) }}
    </div>
  @endif
  @if (Session::has('error_message'))
    <div class="span8">
      {{ Alert::error(Session::get('error_message')) }}
    </div>
  @endif

    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Titulo</th>
                <th>Fecha de publicacion</th>
            </tr>
        </thead>
        <tbody>
    @foreach ($posts->results as $post)
        <tr>
            <td>{{ $post->post_title }}</td>
            <td><span class="badge badge-success">Posted {{$post->updated_at}}</span></td>
            <td>
                {{ HTML::link('admin/post/edit/'.$post->id, 'Editar', array('class' => 'btn btn-success')) }}
                <a class="delete_toggler btn btn-danger" rel="{{$post->id}}">Eliminar</a>

            </td>
		</tr>
        
    @endforeach
        <tbody>
    </table>

<div class="modal hide fade" id="delete_page">
      <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <h3>Are You Sure?</h3>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this page?</p>
      </div>
      <div class="modal-footer">
        {{ Form::open('admin/post', 'DELETE') }}
        <a data-toggle="modal" href="#delete_page" class="btn">Keep</a>
        <input type="hidden" name="id" id="postvalue" value="" />
        <input type="submit" class="btn btn-danger" value="Delete" />
        {{ Form::close() }}
      </div>
    </div>

    <script>
      $('#delete_page').modal({
        show:false
      }); // Start the modal

      // Populate the field with the right data for the modal when clicked
      $('.delete_toggler').each(function(index,elem) {
          $(elem).click(function(){
            $('#postvalue').attr('value',$(elem).attr('rel'));
            $('#delete_page').modal('show');
          });
      });
    </script>
@endsection

@section('pagination')
    	<div class="row">
    		<div class="span8">
	    		{{ $posts -> links(); }}
	   		 </div
		</div>
@endsection