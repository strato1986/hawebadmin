<?php

class Category extends Eloquent {

	public function posts()
	{
		return $this->has_many('Post');
	}

	public static function get_select_items()
	{
		$categories = Category::all();
		$items = array();


		foreach($categories as $category)
		{
			$items[$category->id] = $category->name;
		}

		return $items;
	}
}
