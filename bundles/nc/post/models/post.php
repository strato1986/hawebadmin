<?php

class Post extends Eloquent {

	//public $includes = array('featuredimage');

	public function user()
	{
		return $this->belongs_to('User','post_author');
	}

	public function category()
	{
		return $this->belongs_to('Category');
	}

	
	public function featuredimage()
	{
		return $this->has_one('Multimedia');
	}

	public function get_featured_image()
	{
		$multimedia = Multimedia::find($this->multimedia_id);	
		if ($multimedia)
			return $multimedia;

		return false;
	}

	public function get_thumbnail($value='')
	{
		$featured_image = $this->get_featured_image();
		if ($featured_image != null)
		{

			return $featured_image->get_version($value);
		}

		return new Version();
	}
}
