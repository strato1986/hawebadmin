<?php

class Post_Install_Post {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function($table)
		{
			$table->engine = 'InnoDB';
		    $table->increments('id');
		    $table->string('post_title', 255);
		    $table->text('post_body');
		    $table->integer('category_id');
		    $table->integer('post_author')->unsigned();
		    $table->timestamps();
		    $table->foreign('post_author')->references('id')->on('users');
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}