<?php

class Multimedia extends Eloquent {

	public function versions()
	{
		return $this->has_many('Version');
	}

	public function posts()
	{
		return $this->belongs_to('Posts');
	}
	
	public function pages()
	{
		return $this->belongs_to('Page');
	}

	public function get_version($size='')
	{
		return $this->versions()->where('size','=',$size)->first();
	}

	public static function upload_file($key,$file,$filename = null,$overwrite = false)
	{
		$extension = File::extension($file['name']);
		$directory = path('public').'uploads/';
		$filename = $filename ? $filename : $file['name'];

		if ($overwrite)
		{
			$file = $directory.basename($file['name']);
			if (file_exists($file))
				// Elimina el logo actual
        		unlink($file);
		}

		$upload_success = Input::upload($key, $directory, strtolower($filename));

		if ($upload_success)
		{
			return array(
				'path'	=> $directory . "/" . $filename,
				'extension' => static::get_file_property($directory.$filename,'extension'),
				'name'	=> static::get_file_property($directory.$filename,'filename')
			);
		}
		else
		{
			return null;
		}
	}

	private static function get_file_property($filename,$propery)
	{
		$parts = pathinfo($filename);
		return strtolower($parts[$propery]);
	}
}
