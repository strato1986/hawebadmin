<?php

class Version extends Eloquent {


	public function multimedia()
	{
		return $this->belongs_to('Multimedia');
	}

	public function make($upload=array(),$size=array())
	{
		$directory = path('public').'uploads/';

		$filename_thumb = $upload['name'] . "_" . $size[0] . "x" . $size[1] . "." . $upload['extension'];
		$success = Resizer::open($upload['path'])
		        ->resize($size[0], $size[1], 'crop' )
		        ->save( $directory .'/'. $filename_thumb , 90 );
		if ($success)
		{
			$this->set_attribute('size', $size[0].'x'.$size[1]);
			$this->set_attribute('url', asset('uploads/' . $filename_thumb));
			$this->set_attribute('path', $directory . '/' . $filename_thumb);
		}
	}
}
