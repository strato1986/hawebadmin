<?php

class Multimedia_Create_Multimedia {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('multimedias',function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('url');
			$table->string('path');
			$table->integer('post_id')->unsigned()->nullable();
			$table->integer('page_id')->unsigned()->nullable();
			$table->timestamps();
			$table->foreign('post_id')->references('id')->on('posts')->on_delete('SET NULL');
			$table->foreign('page_id')->references('id')->on('pages')->on_delete('SET NULL');
		});

	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('multimedias');
	}

}