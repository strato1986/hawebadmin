<?php

class Multimedia_Create_Versions {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('versions',function($table)
		{
			$table->increments('id');
			$table->string('url');
			$table->string('path');
			$table->string('size');
			$table->integer('multimedia_id');
			$table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('versions');
	}

}