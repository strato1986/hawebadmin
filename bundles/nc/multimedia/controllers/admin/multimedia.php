<?php

class Multimedia_Admin_Multimedia_Controller extends Base_Controller {

	public $restful = true;

	public function __construct()
	{
		parent::__construct();

		$this->filter('before', 'csrf')->on('post');
	}
	public function action_index()
	{
		// code here..

		return View::make('multimedia::admin.multimedia.index');
	}

	public function post_delete()
	{
		$multimedia = Multimedia::find(Input::get('id'));
		$path = $multimedia->path;
		if($multimedia->delete())
		{
			unlink($path);
			return Response::json(array('success' => 'true'));
		}
	}
}
