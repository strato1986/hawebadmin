<?php

class Backend_Controller extends Base_Controller {

	public function __construct()
	{
		parent::__contruct();
		$this->filter('before', 'csrf')->on('post');
		
	}

	public function build_nav_menu()
	{
		return Request::route()->controller;
	}

}
