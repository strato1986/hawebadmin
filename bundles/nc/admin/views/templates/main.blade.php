<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Blog</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
    {{ Asset::container('bootstrapper')->styles(); }}
    
    <style>
      body {
        /*padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    {{ Asset::container('bootstrapper')->scripts(); }} 
    {{ HTML::script('js/validate.js') }}
    {{ HTML::script('js/jquery.form.js') }}
    @yield('assets')
    
</head>
<body>

    {{ Navbar::create('Backend', URL::to('/admin'),
    array(
      array(
        'attributes' => array(),
          'items' => array(
            '|||',
            array('label'=>'Ver Sitio', 'url' => URL::to('/')),
          )
        ),
        array(
          'attributes' => array(
            'class' => 'pull-right'),
            'items' => array(
              array(
                'label'=> Sentry::user()->get('metadata.first_name') . ' ' . Sentry::user()->get('metadata.last_name'), 'url'=>'#',
                'items'=> array(
                  array(
                    'label'=>'Editar Perfil', 
                    'url'=>'admin/user'
                    ),
                    '---',
                    array(
                      'label'=>'Cerrar Sesion', 
                      'url'=>'user/logout'
                    ),
                )
              )
            )
        )
      ), Navbar::FIX_TOP, true, array('class' => 'navbar-inverse')
    )
    }}

    <div class="container">
          <div class="row">
            <div class="span3">
                  {{ $menu }}
              
            </div>
            <div class="span9">
              @yield('content')
              @yield('pagination')
            </div>
            
          </div>
          
    </div><!--/container-->

    <div class="container">
        <footer>
            <p>Blog &copy; 2012</p>
        </footer>
      </div>
</body>
</html>