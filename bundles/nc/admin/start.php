<?php

/*
|--------------------------------------------------------------------------
| Auto-Loader Mappings
|--------------------------------------------------------------------------
|
| Laravel uses a simple array of class to path mappings to drive the class
| auto-loader. This simple approach helps avoid the performance problems
| of searching through directories by convention.
|
| Registering a mapping couldn't be easier. Just pass an array of class
| to path maps into the "map" function of Autoloader. Then, when you
| want to use that class, just use it. It's simple!
|
*/

Autoloader::map(array(

));

/*
|--------------------------------------------------------------------------
| Auto-Loader Directories
|--------------------------------------------------------------------------
|
| The Laravel auto-loader can search directories for files using the PSR-0
| naming convention. This convention basically organizes classes by using
| the class namespace to indicate the directory structure.
|
*/

Autoloader::directories(array(

));

View::composer('admin::templates.main', function($view) {

	$items = array(
		array('header'	,'General'),
		array('admin'	,'Inicio', '/admin', 'home', 'icon-white'),
		array('header'	,'Contenidos'),
		array('post'	,'Blog', '/admin/post', 'file'),
		array('page'	,'Paginas', '/admin/page', 'book'),
        array('document','Documentos', '/admin/document', 'folder-open'),
		array('calendar','Calendario', '/admin/calendar', 'calendar'),
		array('settings','Preferencias', '/admin/settings', 'cog')
	);

    $menu = array();

    $controller_name = explode('.', Request::route()->controller);

    if($controller_name[0] == '')
    	$controller_name = 'admin';
    else
    	$controller_name = $controller_name[1];

    foreach($items as $item)
    {    	
    	
    	if ($item[0] == 'header')
    		$menu[] = array($item[0] => $item[1]);
    	else
    	{
    		$array = array('label' => $item[1],'url' => $item[2], 'icon' => $item[3]);
    		if (isset($item[4]))
    		{
    			$array['attributes'] = $item[4];
    		}

    		if ($controller_name == $item[0])
    			$array['active'] = true;
    		$menu[] = $array;
    	}
    	
    }

	$data = array(
		'menus'	=> $menu
	);
    $view->nest('menu', 'admin::templates._menu', array('data' => $data));
});