<?php
Route::get('page/(:any)','page::page@index');
/*
Route::get('/page/(:any)', function($slug){
	$page = Page::where('slug','=',$slug)->first();
	if ($page)
		return View::make('page::page.view')->with('page',$page);
	return Redirect::to('/');
});
*/
// Route for Page_Admin_Page_Controller
Route::controller('page::admin.page');

// Route for Page_Page_Controller
//Route::controller('page::page');