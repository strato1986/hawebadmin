<?php

class Page_Create_Pages {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages', function($t){
			$t->increments('id');
			$t->string('page_title',255);
			$t->text('page_body');
			$t->string('slug');
			$t->integer('page_author')->unsigned();
			$t->timestamps();
			$t->foreign('page_author')->references('id')->on('users');
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}