<?php

class Page extends Eloquent {

	public static $sluggable = array(
    'build_from' => 'page_title',
    'save_to'    => 'slug',
  );
  
	public function user()
	{
		return $this->belongs_to('User','page_author');
	}

	public function featuredimage()
	{
		return $this->has_one('Multimedia');
	}

}
