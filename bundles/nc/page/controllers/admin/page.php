<?php

class Page_Admin_Page_Controller extends Backend_Controller {

	public $restful = true;

	private $rules = array(
			'page_title'	=> 'required',
			'page_body'		=> 'required'
		);

	public function get_index()
	{
		$pages = Page::order_by('created_at', 'desc')->paginate(5);

		return View::make('page::admin.page.index')->with('pages', $pages);
	}

	public function get_new()
	{
		$form = Formly::make();
		$form->display_inline_errors = true;
		$form->form_class = 'form_vertical';
		$data = array(
			'user' => Sentry::user(),
			'form' => $form,
			'create' => true,
		);
		return View::make('page::admin.page.form', $data);
	}

	public function get_edit($id = null)
	{
		if (!$id) return Redirect::to('admin');
		$page = Page::with('user')->find($id);
		if (!$page) return Redirect::to('admin');

		$form = Formly::make();
		$form->display_inline_errors = true;
		$form->form_class = 'form_vertical';

		$this->data = array(
			'user'	=>	Sentry::user(),
			'form'	=> $form,
			'page'	=>	$page,
			'featuredimage' => $page->featuredimage
		);

		return View::make('page::admin.page.form',$this->data);
	}

	public function post_new()
	{
		$this->filter('before', 'csrf')->on('post');
		
		
		$validator = Validator::make(Input::get(), $this->rules);

		if ($validator->fails())
		{
			return Redirect::to('admin/page/new')->with_input()->with_errors($validator);
		} 
		$new_page = array(
			'page_title'	=> Input::get('page_title'),
			'page_body'		=> Input::get('page_body'),
			'page_author'	=> Input::get('page_author')
		);
		$page = new Page($new_page);
		$page->save();

		$featured_image = Input::file('featured_image');

		if (Input::has_file('featured_image') and ($featured_image['size'] > 0 ))
		{
			
			$user = Sentry::user();
			
			
			$upload = Multimedia::upload_file('featured_image',$featured_image,null,true);

			if ($upload)
			{
				$directory = path('public').'uploads/';
				$filename = $upload['name'].'.'.$upload['extension'];

				$multimedia = new Multimedia();
				$multimedia->url = asset('uploads/' . $filename);
				$multimedia->path = $directory . $filename;
				$multimedia->save();


				$version50x50 = new Version();
				$version50x50->make($upload,array(50,50));
				$version50x50->save();
			    
			    $multimedia->versions()->insert($version50x50);

			    $version730x365 = new Version();
				$version730x365->make($upload,array(730,365));
				$version730x365->save();
			    
			    $multimedia->versions()->insert($version730x365);

			    $page->featuredimage()->insert($multimedia);
			}
		
		}
		return Redirect::to('admin/page')->with('success_message','Datos guardados correctamente');
	}

	public function post_edit()
	{

		$v = Validator::make(Input::all(), $this->rules);
		$id = Input::get('id');
		if ($v->fails())
		{
			return Redirect::to('admin/page/edit/'.$id)
				->with('user', Sentry::user())
				->with_errors($v)
				->with_input();
		}

		$page = Page::find($id);
		$page->page_title = Input::get('page_title');
		$page->page_body = Input::get('page_body');

		$featured_image = Input::file('featured_image');

		if (Input::has_file('featured_image') and ($featured_image['size'] > 0 ))
			{
					$user = Sentry::user();
					
					

					$upload = Multimedia::upload_file('featured_image',$featured_image,null,true);

					if ($upload)
					{

						$directory = path('public').'uploads/';
						$filename = $upload['name'].'.'.$upload['extension'];

						$multimedia = new Multimedia();
						$multimedia->url = asset('uploads/' . $filename);
						$multimedia->path = $directory . $filename;
						$multimedia->save();

						$version50x50 = new Version();
						$version50x50->make($upload,array(50,50));
						$version50x50->save();

						$multimedia->versions()->insert($version50x50);

					  $version730x365 = new Version();
						$version730x365->make($upload,array(730,365));
						$version730x365->save();

						$multimedia->versions()->insert($version730x365);

						$page->featuredimage()->insert($multimedia);
						
						
					}
			}
		$page->save();

		return Redirect::to('admin/page');


	}
	public function post_delete($id = null)
	{
		$rules = array(
			'id'	=> 'required|exists:pages'
		);
		$validation = Validator::make(Input::all(),$rules);

		if ($validation->fails())
		{
			return Redirect::to('admin/page'.$this->views.'');
		} else {
			$page = Page::with('user')->find(Input::get('id'));
			$page->delete();

			return Redirect::to('admin/page'.$this->views.'')->with('success_message',true);
		}
	}

}
