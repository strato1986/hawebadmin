<?php

class Page_Page_Controller extends Frontend_Controller {



	public function action_index($slug = '')
	{
		if(!$slug)
			return Redirect::to('/');
		$page = Page::where('slug','=',$slug)->first();
		if (!$page)
			return Redirect::to('/');

		return View::make('page::page.view')->with('page',$page);
		
	}

}
