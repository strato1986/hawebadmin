@layout('templates.frontend')
@section('content')

	<!-- BEGIN .post -->
		<div class="post-100 post type-post status-publish format-image hentry category-cities category-travel tag-italy odd" id="post-100">

		<!-- BEGIN .entry-header -->
		<div class="entry-header">
		
					
			<h1><span class="post-icon"></span>{{ $page->post_title }}</h1>
			
								
		</div>
		<!-- BEGIN .entry-header -->
		
		<!-- BEGIN .entry-meta -->
		<div class="entry-meta">
		
			<p>Por {{ $page->user->username }} · {{ strftime(' %a %d, %Y', strtotime($page->created_at)) }} · <!--<a href="http://www.awesem.com/deadline-responsive/2012/05/28/schenna/#respond" rel="nofollow" title="Comment on Schenna">Sin comentarios</a>-->
			</p>
			
		</div>
		<!-- END .entry-meta -->
			
		@if($page->featuredimage)
			<!-- BEGIN .entry-thumb -->
			<div class="entry-thumb">
						
				<img src="{{ $page->featuredimage->url }}" class="attachment-single-project-thumbnail wp-post-image" alt="schenna">		
			</div>
			<!-- END .entry-thumb -->
		@endif
				
		<!-- BEGIN .entry -->
		<div class="entry">
						
			{{ $page->post_body }}
		
		</div>
		<!-- END .entry -->
			
	</div>
	<!-- END .post -->
@endsection