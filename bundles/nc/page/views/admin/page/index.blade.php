@layout('admin::templates.main')
@section('content')	
  <h1>Paginas</h1>
  <a href="{{ URL::to('admin/page/new') }}">Agregar pagina</a>
  @if (Session::has('success_message'))
    <div class="span8">
      {{ Alert::success(Session::get('success_message')) }}
    </div>
  @endif
  @if (Session::has('error_message'))
    <div class="span8">
      {{ Alert::error(Session::get('error_message')) }}
    </div>
  @endif

    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Titulo</th>
                <th>Fecha de Publicacion</th>
            </tr>
        </thead>
        <tbody>
    @foreach ($pages->results as $page)
        <tr>
            <td>{{ $page->page_title }}</td>
            <td><p class="badge badge-success">Creado {{ $page->updated_at }}</p></td>
            <td>
              {{ HTML::link('admin/page/edit/'.$page->id, 'Editar', array('class' => 'btn btn-success')) }}
                <a class="delete_toggler btn btn-danger" rel="{{$page->id}}">Eliminar</a>
            </td>
    		
		</tr>
        
    @endforeach
        <tbody>
    </table>

    <div class="modal hide fade" id="delete_page">
      <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <h3>Are You Sure?</h3>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this page?</p>
      </div>
      <div class="modal-footer">
        {{ Form::open('admin/page/delete', 'POST') }}
        <a data-toggle="modal" href="#delete_page" class="btn">Keep</a>
        <input type="hidden" name="id" id="postvalue" value="" />
        <input type="submit" class="btn btn-danger" value="Delete" />
        {{ Form::close() }}
      </div>
    </div>

    <script>
      $('#delete_page').modal({
        show: false
      }); // Start the modal

      // Populate the field with the right data for the modal when clicked
      $('.delete_toggler').each(function(index,elem) {
          $(elem).click(function(){
            $('#postvalue').attr('value',$(elem).attr('rel'));
            $('#delete_page').modal('show');
          });
      });
    </script>
@endsection

@section('pagination')
    	<div class="row">
    		<div class="span8">
	    		{{ $pages->links(); }}
	   		 </div
		</div>
@endsection