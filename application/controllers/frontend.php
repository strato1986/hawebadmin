<?php

class Frontend_Controller extends Base_Controller {

	public function __construct()
	{
		parent::__contruct();

		$calendars = Calendar::order_by('due_date','ASC')->take(5)->get();
		View::share('site_title',Setting::get('site_title'));
		View::share('calendars',$calendars);
	}

}
