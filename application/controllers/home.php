<?php

class Home_Controller extends Frontend_Controller {

	/*
	|--------------------------------------------------------------------------
	| The Default Controller
	|--------------------------------------------------------------------------
	|
	| Instead of using RESTful routes and anonymous functions, you might wish
	| to use controllers to organize your application API. You'll love them.
	|
	| This controller responds to URIs beginning with "home", and it also
	| serves as the default controller for the application, meaning it
	| handles requests to the root of the application.
	|
	| You can respond to GET requests to "/home/profile" like so:
	|
	|		public function action_profile()
	|		{
	|			return "This is your profile!";
	|		}
	|
	| Any extra segments are passed to the method as parameters:
	|
	|		public function action_profile($id)
	|		{
	|			return "This is the profile for user {$id}.";
	|		}
	|
	*/

	public function action_index()
	{

		//$posts = Post::where('category_id','=','1')->take(4)->get();
		

		$noticias = Post::where('category_id','=','1')->take(5)->get();
		$posts_eventos = Post::where('category_id','=','2')->take(5)->get();
		
		$transparencias = Document::where('category_id','=','4')->take(5)->get();
		$tramites = Document::where('category_id','=','5')->take(5)->get();

		return View::make('home.index')
			->with('noticias',$noticias)
			->with('posts_eventos', $posts_eventos)
			->with('transparencias', $transparencias)
			->with('tramites',$tramites);
		//return View::make('home.test')->with('noticias',$noticias);
	}

}