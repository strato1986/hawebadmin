<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used
	| by the validator class. Some of the rules contain multiple versions,
	| such as the size (max, min, between) rules. These versions are used
	| for different input types such as strings and files.
	|
	| These language lines may be easily changed to provide custom error
	| messages in your application. Error messages for custom validation
	| rules may also be added to this file.
	|
	*/

	"accepted"       => "El :attribute debe ser aceptado.",
	"active_url"     => "El :attribute no es una URL válida.",
	"after"          => "El :attribute debe ser una fecha despues de :date.",
	"alpha"          => "El :attribute solo puede contener letras.",
	"alpha_dash"     => "El :attribute solo puede contener letras, numeros, y guiones.",
	"alpha_num"      => "El :attribute solo puede contener letras y numeros.",
	"array"          => "El :attribute debe tener un elemento seleccionado.",
	"before"         => "El :attribute debe ser una fecha anterior a :date.",
	"between"        => array(
		"numeric" => "El :attribute debe estar entre :min - :max.",
		"file"    => "El :attribute debe estar entre :min - :max kilobytes.",
		"string"  => "El :attribute debe estar entre :min - :max characters.",
	),
	"confirmed"      => "El :attribute no coincide.",
	"count"          => "El :attribute debe tener exactamente :count elementos seleccionados.",
	"countbetween"   => "El :attribute debe tener entre :min y :max elementos seleccionados.",
	"countmax"       => "El :attribute must have less than :max selected elements.",
	"countmin"       => "El :attribute must have at least :min selected elements.",
	"different"      => "El :attribute and :other must be different.",
	"email"          => "El :attribute format is invalid.",
	"exists"         => "El selected :attribute is invalid.",
	"image"          => "El :attribute must be an image.",
	"in"             => "El selected :attribute is invalid.",
	"integer"        => "El :attribute must be an integer.",
	"ip"             => "El :attribute must be a valid IP address.",
	"match"          => "El :attribute format is invalid.",
	"max"            => array(
		"numeric" => "El :attribute must be less than :max.",
		"file"    => "El :attribute must be less than :max kilobytes.",
		"string"  => "El :attribute must be less than :max characters.",
	),
	"mimes"          => "El :attribute debe ser un archivo del tipo: :values.",
	"min"            => array(
		"numeric" => "El :attribute must be at least :min.",
		"file"    => "El :attribute must be at least :min kilobytes.",
		"string"  => "El :attribute must be at least :min characters.",
	),
	"not_in"         => "El selected :attribute is invalid.",
	"numeric"        => "El :attribute must be a number.",
	"required"       => "El campo :attribute es requerido.",
	"same"           => "El :attribute and :other must match.",
	"size"           => array(
		"numeric" => "El :attribute must be :size.",
		"file"    => "El :attribute must be :size kilobyte.",
		"string"  => "El :attribute must be :size characters.",
	),
	"unique"         => "El :attribute has already been taken.",
	"url"            => "El :attribute format is invalid.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute_rule" to name the lines. This helps keep your
	| custom validation clean and tidy.
	|
	| So, say you want to use a custom validation message when validating that
	| the "email" attribute is unique. Just add "email_unique" to this array
	| with your custom message. The Validator will handle the rest!
	|
	*/

	'custom' => array(),

	/*
	|--------------------------------------------------------------------------
	| Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as "E-Mail Address" instead
	| of "email". Your users will thank you.
	|
	| The Validator class will automatically search this array of lines it
	| is attempting to replace the :attribute place-holder in messages.
	| It's pretty slick. We think you'll like it.
	|
	*/

	'attributes' => array(
		'event_title'	=> 'Titulo',
		'due_date'		=> 'Fecha de Vencimiento',
		'document_title'	=>	'Titulo',
		'document_description'	=>	'Descripcion',
		'document_file'		=>	'Documento'
	),

);