@layout('templates.frontend')
@section('content')

			<!-- BEGIN .container -->
			<div class="container">
				<div id="widgets-homepage-fullwidth" class="grid-8">
	<div class="widget aw_sliderblog_widget" id="aw_sliderblog_widget-2">
	
		@include('blocks.slider')
		<script>
			jQuery(document).ready(function($){
				$("#slider-aw_sliderblog_widget-2").flexslider({
					animation: 'slide',
										slideshow: true,
					slideshowSpeed: 5000,
										animationDuration: 500,
					directionNav: false,
										controlNav: true,
															keyboardNav: true,
															mousewheel: false,
															randomize: false,
										controlsContainer: '.wrap-aw_sliderblog_widget-2'
				});
			    				$('#aw_sliderblog_widget-2 .slider-caption').css('display', 'none');
				$('#aw_sliderblog_widget-2').hover(function() {
			        $('#aw_sliderblog_widget-2 .slider-caption').stop().fadeIn('fast')
			    }, function() {
			        $('#aw_sliderblog_widget-2 .slider-caption').stop().fadeOut('fast')
			    });
			});
		</script>
		
		</div>
	<div id="aw_latestfeaturedposts_widget-2" class="widget aw_latestfeaturedposts_widget">		
		<!-- BEGIN .container -->
		<div class="container">
			@include('blocks.noticias')

			@include('blocks.eventos')

			<div class="clear"></div>

			@include('blocks.transparencia')

			@include('blocks.tramites')

			<div class="clear"></div>
		</div>
		<!-- END .container -->
	</div>


</div>
<!-- END #widgets-homepage-fullwidth -->
				</div>
			<!-- END .container -->
@endsection