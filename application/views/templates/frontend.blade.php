<!DOCTYPE html>   
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]> <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]> <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]> <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html dir="ltr" lang="en-US" class="no-js"> <!--<![endif]-->


<!-- BEGIN head -->
<head>
	<!-- Title -->
	<title>{{ $site_title }} </title>
	
	<!-- Meta Tags -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	
	<!-- Favicon & Mobileicon -->
	<link rel="shortcut icon" href="http://www.awesem.com/deadline-responsive/wp-content/themes/deadline-responsive/favicon.ico" />
	<link rel="apple-touch-icon" href="http://www.awesem.com/deadline-responsive/wp-content/themes/deadline-responsive/mobileicon.png" />
	
	<!-- JS -->
	<script type='text/javascript' src='{{ URL::to_asset('js/modernizr.js') }}'></script>
	<script type='text/javascript' src='{{ URL::to_asset('js/jquery-1.8.1.min.js') }}'></script>
	
	<!-- CSS -->
	<link rel="stylesheet" href="{{ URL::to_asset('css/style.css') }}" media="screen" />
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noticia+Text:400,700italic,700,400italic" media="screen" />
	<link rel="stylesheet" href="{{ URL::to_asset('css/animation.min.css') }}" media="screen" />
	<link rel="stylesheet" href="{{ URL::to_asset('css/tooltip.min.css') }}" media="screen" />
	<link rel="stylesheet" href="{{ URL::to_asset('css/custom2.css') }}" media="screen" />
	<link rel="stylesheet" href="{{ URL::to_asset('css/custom.css') }}" media="screen" />
	<!--[if IE 8]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
    <![endif]-->
    <style>
    	body {
    		background: url("{{ asset('uploads').'/fondo1.jpg' }}") #f2f2f2;
    		background-position: 50% 13px;
    		background-repeat: no-repeat no-repeat;
    	}
    </style>
</head>
	<body class="home page page-id-7 page-template page-template-template-homepage-php gecko">
	<!-- BEGIN #top-nav -->
	<nav id="top-nav" class="menu-nav">
	
		<!-- BEGIN .container -->
		<div class="container">
		
					
			<!-- BEGIN .grid-9 -->
			<div class="grid-9">
				<ul id="menu-top" class="menu">
					<li id="menu-item-9" class="menu-item">
						<a href="http://www.awesem.com/deadline-responsive/blog/">Turismo</a>
					</li>
					<li id="menu-item-90" class="menu-item"><a href="http://www.awesem.com/deadline-responsive/archives/">Transparencia</a></li>
					<li id="menu-item-89" class="menu-item"><a href="http://www.awesem.com/deadline-responsive/full-width/">Ordenanzas</a></li>
				
				</ul>
			</div>
			<!-- END .grid-9 -->

			<!-- BEGIN #date -->
			<div id="date" class="grid-3" style="float: right">
				<p class="text-right margin-0">
					<span class="rounded">{{ strftime('%a %d %b %Y')}}</span>
				</p>
			</div>
			<!-- END #date -->
				
			<div class="clear"></div>
		</div>
		<!-- END .container -->

	</nav>
	<!-- END #top-nav -->

	<!-- BEGIN #wrapper -->
	<div id="wrapper" class="container">
		<!-- BEGIN header -->
		<header>
		
						
			<div class="header-wrapper">
								
				<!-- BEGIN #logo -->

				<div id="logo" class="leaderboard-true">
				
										<a href="{{ URL::to('/') }}"><img src="{{ Setting::get_logo()}}" alt="{{ $site_title }}" /></a>
									
				</div>
				<!-- END #logo -->
				
				<!-- BEGIN #leaderboard
				<div id="leaderboard">
				
					<a href="http://www.awesemthemes.com/"><img src="http://cdn.awesem.com/images/728x90.png" alt="" /></a>				
				</div>
				 END #leaderboard 
				
				<div class="clear"></div>-->
			
			</div>
			
						
			<div class="clear"></div>
			
			<!-- BEGIN #main-nav -->
			<nav id="main-nav" class="grid-12 menu-nav">
			
				<div id="main-navIcon">Menu</div>

				<div class="menu-main-container">
					<ul id="menu-main" class="menu">
						<li class="menu-item"><a href="{{ URL::to('/') }}">Inicio</a>
						</li>
						<li class="menu-item"><a href="#">Tolhuin</a>
							<ul class="sub-menu">
								<li class="menu-item"><a href="#">Historia</a></li>
								<li class="menu-item"><a href="#">Geografia</a></li>
								<li class="menu-item"><a href="#">Clima</a></li>
								<li class="menu-item"><a href="#">Multimedia</a></li>
								<li class="menu-item"><a href="#">Aniversario</a></li>
							</ul>
						</li>
						<li class="menu-item"><a href="#">La Comuna</a>
							<ul class="sub-menu">
								<li class="menu-item"><a href="#">Sr Intendente</a></li>
								<li class="menu-item"><a href="#">Gabinete Comunal</a></li>
								<li class="menu-item"><a href="#">Concejo Deliberante</a></li>
							</ul>
						</li>
						<li class="menu-item"><a href="#">Tramites</a></li>
						<li class="menu-item"><a href="#">Informacion</a></li>
						<li class="menu-item"><a href="#">Noticias</a></li>
						<li class="menu-item"><a href="#">Contacto</a></li>
					</ul>
				</div>				
				<div class="clear"></div>
            
			</nav>
			<!-- END #main-nav -->
			
			<div class="clear"></div>
		
		</header>
		<!-- END header -->

		<!-- BEGIN .grid-8 -->
		<div class="grid-8">

			@yield('content')

		</div>
		<!-- END .grid-8 -->
			
		@include('blocks.sidebar')
		<div class="clear"></div>


	</div>
	<!-- END #wrapper -->
		@include('blocks.footer');
		<script type='text/javascript' src='{{ URL::to_asset("js/easing.js") }}'></script>
		<script type='text/javascript' src='{{ URL::to_asset("js/superfish.js") }}'></script>
		<script type='text/javascript' src='{{ URL::to_asset("js/validate.js") }}'></script>
		<script type='text/javascript' src='{{ URL::to_asset("js/touchwipe.js") }}'></script>
		<script type='text/javascript' src='{{ URL::to_asset("js/caroufredsel.js")}}'></script>
		<script type='text/javascript' src='{{ URL::to_asset("js/flexslider.js")}}'></script>
		<script type='text/javascript' src='{{ URL::to_asset("js/jplayer.js")}}'></script>
		<script type='text/javascript' src='{{ URL::to_asset("js/fitvids.js")}}'></script>
		<!--<script type='text/javascript' src='{{ URL::to_asset("js/gmap.js")}}'></script>-->
		<script type='text/javascript' src='{{ URL::to_asset("js/effects.js")}}'></script>
		<script type='text/javascript' src='{{ URL::to_asset("js/tooltip.min.js")}}'></script>
		<script type='text/javascript' src='{{ URL::to_asset("js/custom.js")}}'></script>

	</body>
</html>