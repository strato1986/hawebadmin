<!-- BEGIN #sidebar -->
<html>
	<head>
		<title></title>
	</head>
	<body>
		<div class="grid-4" id="sidebar">
			<div class="widget widget_search" id="search-2">
				<!-- BEGIN #searchform -->
				<form action="http://www.awesem.com/deadline-responsive/" id="searchform" method="get">
					<label for="s" class="none">Busqueda:</label>
					<div class="input-wrapper">
						<input type="text" placeholder="Busqueda:" id="s" name="s" value="">
					</div><input type="submit" value="Search" id="searchsubmit">
				</form><!-- END #searchform -->
			</div>
			<div class="widget aw_socialcounter_widget" id="aw_socialcounter_widget-2">
				<ul>
					<!-- BEGIN .feedburner -->
					<li class="feedburner">
						<img alt="Subscribe RSS Feed" src="http://www.awesem.com/deadline-responsive/wp-content/themes/deadline-responsive/_assets/img/rss.png" class="alignleft"> <a href="#">Suscribase a RSS </a> <span>2583 subscriptos</span>
					</li><!-- END .feedburner -->
					<!-- BEGIN .twitter -->
					<li class="twitter">
						<img alt="Follow us on Twitter" src="http://www.awesem.com/deadline-responsive/wp-content/themes/deadline-responsive/_assets/img/twitter.png" class="alignleft"> <a href="#">Seguí a Tolhuin en Twitter</a> <span>264 seguidores</span>
					</li><!-- END .twitter -->
					<!-- BEGIN .facebook -->
					<li class="facebook">
						<img alt="Connect on Facebook" src="http://www.awesem.com/deadline-responsive/wp-content/themes/deadline-responsive/_assets/img/facebook.png" class="alignleft"> <a href="#">Conectarse con Tolhuin en Facebook</a> <span>98 fans</span>
					</li><!-- END .facebook -->
				</ul>
			</div>
			<div class="widget aw_tabbed_widget" id="aw_tabbed_widget-2">
				<div class="tabs">
					<ul class="nav clearfix">
						<li class="active">
							<a title="Vencimientos" href="#tab-latest-posts" id="link-latest-posts" class='tool'>Vencimientos</a>
						</li>
						<li>
							<a id="link-tags" href="#tab-pago" title="Lugares de pago" class="tool">Lugares de pago</a>
						</li>
					</ul>
					<div class="tab" id="tab-latest-posts" style="display:block">
						<div class="floated-thumb">

							@forelse($calendars as $calendar)
							<?php
								$calendar_attr = $calendar->compare_date_with_now();
							?>
							@if($calendar_attr['css_class'])
								<!-- BEGIN .post-thumb -->
								<div class="post-thumb">
									<a class="calendar_date" title="Permalink to Nice" href="#">
										<div class="{{ $calendar_attr['css_class'] }}">{{ $calendar->short_date() }}</div>
									</a>
								</div><!-- END .post-thumb -->
								<!-- BEGIN .post-meta -->
								<div class="post-meta">
									<p>
										<a title="Permalink to Nice" href="#" class="meta-title">{{ $calendar->event_title }}</a><br>
										<!--
										May 28, 2012 · <a title="Comment on Nice" rel="nofollow" href="http://www.awesem.com/deadline-responsive/2012/05/28/nice/#comments">5 comments</a>-->
									</p>
								</div><!-- END .post-meta -->
								<div class="clear"></div><!-- BEGIN .post-thumb -->
							@endif
							@empty
								<p>
								No hay fechas cercanas
							</p>
							@endforelse



						</div>
					</div>
					<div id="tab-pago" class="tab" style="display: none; "><div class="floated-thumb">						
												
						<!-- BEGIN .post-thumb -->
						<div class="post-thumb">
							
							<a href="#" title="Permalink to Nice"><img src="http://www.awesem.com/deadline-responsive/files/2012/05/nice-50x50.jpg" class="attachment-grid-1 wp-post-image" alt="nice"></a>
							
						</div>
						<!-- END .post-thumb -->
								
						<!-- BEGIN .post-meta -->
						<div class="post-meta ">
						
							<p><a class="meta-title" href="#" title="Permalink to Nice">Lugar #1</a></p>
								
						</div>
						<!-- END .post-meta -->
						
						<div class="clear"></div>	
																		
												
						<!-- BEGIN .post-thumb -->
						<div class="post-thumb">
							
							<a href="#" title="Permalink to Big Buck Bunny (Self hosted)"><img src="http://www.awesem.com/deadline-responsive/files/2012/05/big-buck-bunny-50x50.png" class="attachment-grid-1 wp-post-image" alt="big-buck-bunny"></a>
							
						</div>
						<!-- END .post-thumb -->
						
												
						<!-- BEGIN .post-meta -->
						<div class="post-meta ">
						
							<p><a class="meta-title" href="#" title="Permalink to Big Buck Bunny (Self hosted)">Lugar #2</a></p>
								
						</div>
						<!-- END .post-meta -->
						
						<div class="clear"></div>	
														
											
												
						<!-- BEGIN .post-thumb -->
						<div class="post-thumb">
							
							<a href="#" title="Permalink to Apple becomes world’s most valuable company"><img src="http://www.awesem.com/deadline-responsive/files/2012/05/apple-50x50.jpg" class="attachment-grid-1 wp-post-image" alt="apple"></a>
							
						</div>
						<!-- END .post-thumb -->
						
												
						<!-- BEGIN .post-meta -->
						<div class="post-meta ">
						
							<p><a class="meta-title" href="#" title="Permalink to Apple becomes world’s most valuable company">Lugar #3</a></p>
								
						</div>
						<!-- END .post-meta -->
						
						<div class="clear"></div>	
														
											
												
						<!-- BEGIN .post-thumb -->
						<div class="post-thumb">
							
							<a href="#" title="Permalink to Italy"><img src="http://www.awesem.com/deadline-responsive/files/2012/05/italy-1-50x50.jpg" class="attachment-grid-1 wp-post-image" alt="italy-1"></a>
							
						</div>
						<!-- END .post-thumb -->
						
												
						<!-- BEGIN .post-meta -->
						<div class="post-meta ">
						
							<p><a class="meta-title" href="#" title="Permalink to Italy">Lugar #4</a></p>
								
						</div>
						<!-- END .post-meta -->
						
						<div class="clear"></div>	
														
											
												
						<!-- BEGIN .post-thumb -->
						<div class="post-thumb">
							
							<a href="#" title="Permalink to AIRMANN – Panic attack"><img src="http://www.awesem.com/deadline-responsive/files/2012/05/airmann-panic-attack-50x50.png" class="attachment-grid-1 wp-post-image" alt="airmann-panic-attack"></a>
							
						</div>
						<!-- END .post-thumb -->
											
						<!-- BEGIN .post-meta -->
						<div class="post-meta ">
						
							<p><a class="meta-title" href="#" title="Permalink to AIRMANN – Panic attack">Lugar #5</a></p>
								
						</div>
						<!-- END .post-meta -->
						
						<div class="clear"></div>	
														
					</div></div>
				</div>
			</div>
			<div class="widget aw_flickr_widget" id="aw_flickr_widget-2">
				<h3 class="widget-title">
					Galeria de fotos
				</h3><script src="http://www.flickr.com/badge_code_v2.gne?count=8&amp;display=random&amp;size=s&amp;layout=x&amp;source=group&amp;group=1654383@N24" type="text/javascript">
</script>
				<div id="flickr_badge_image1" class="flickr_badge_image">
					<a href="#"><img width="75" height="75" title="showcase-simple" alt="Una foto de Flickr" src="http://farm3.staticflickr.com/2436/5725557675_af1449f25e_s.jpg"></a>
				</div>
				<div id="flickr_badge_image2" class="flickr_badge_image">
					<a href="#"><img width="75" height="75" title="showcase-fargo" alt="Una foto de Flickr" src="http://farm4.staticflickr.com/3507/5725556657_0e9778e482_s.jpg"></a>
				</div>
				<div id="flickr_badge_image3" class="flickr_badge_image">
					<a href="#"><img width="75" height="75" title="showcase-lc" alt="Una foto de Flickr" src="http://farm3.staticflickr.com/2001/5726112312_606be73bb6_s.jpg"></a>
				</div>
				<div id="flickr_badge_image4" class="flickr_badge_image">
					<a href="#"><img width="75" height="75" title="showcase-eink1" alt="Una foto de Flickr" src="http://farm6.staticflickr.com/5082/5725556417_c5b37eff47_s.jpg"></a>
				</div>
				<div id="flickr_badge_image5" class="flickr_badge_image">
					<a href="#"><img width="75" height="75" title="showcase-ready" alt="Una foto de Flickr" src="http://farm3.staticflickr.com/2760/5726112634_430dc635ee_s.jpg"></a>
				</div>
				<div id="flickr_badge_image6" class="flickr_badge_image">
					<a href="#"><img width="75" height="75" title="showcase-orm-700x611" alt="Una foto de Flickr" src="http://farm4.staticflickr.com/3106/5726112448_4790759160_s.jpg"></a>
				</div>
				<div id="flickr_badge_image7" class="flickr_badge_image">
					<a href="#"><img width="75" height="75" title="showcase-tgh" alt="Una foto de Flickr" src="http://farm3.staticflickr.com/2580/5726113086_2d0c7806bc_s.jpg"></a>
				</div>
				<div id="flickr_badge_image8" class="flickr_badge_image">
					<a href="#"><img width="75" height="75" title="showcase-ipa" alt="Una foto de Flickr" src="http://farm6.staticflickr.com/5212/5725556925_d23109d841_s.jpg"></a>
				</div><span class="flickr_badge_beacon" style="position:absolute;left:-999em;top:-999em;visibility:hidden"><img width="0" height="0" alt="" src="http://geo.yahoo.com/p?s=792600102&amp;t=2352da6ce3e805c9c754cc1cc642df6a&amp;r=http%3A%2F%2Fwww.awesem.com%2Fdeadline-responsive%2F&amp;fl_ev=0&amp;lang=es&amp;intl=ar"></span>
				<div class="clear"></div>
			</div>
			<!--
			<div class="widget aw_ad300x250_widget" id="aw_ad300x250_widget-2">
				<h3 class="widget-title">
					Our sponsors
				</h3><a href="http://www.awesemthemes.com"><img alt="" src="http://cdn.awesem.com/images/300x250.png"></a>
			</div>
			<div class="widget aw_ad125x125_widget" id="aw_ad125x125_widget-2">
				<div class="ad125x125 ad-1">
					<a href="http://www.awesemthemes.com"><img alt="" src="http://cdn.awesem.com/images/125x125.png"></a>
				</div>
				<div class="ad125x125 ad-2">
					<a href="http://www.awesemthemes.com"><img alt="" src="http://cdn.awesem.com/images/125x125.png"></a>
				</div>
				<div class="ad125x125 ad-3">
					<a href="http://www.awesemthemes.com"><img alt="" src="http://cdn.awesem.com/images/125x125.png"></a>
				</div>
				<div class="ad125x125 ad-4">
					<a href="http://www.awesemthemes.com"><img alt="" src="http://cdn.awesem.com/images/125x125.png"></a>
				</div>
				<div class="clear"></div>
			</div><!-- BEGIN .container 
			<div class="container">
				<!-- BEGIN .grid-2 
				<div class="grid-2">
					<div class="widget aw_ad120x60_widget" id="aw_ad120x60_widget-2">
						<a href="http://www.awesemthemes.com"><img alt="" src="http://cdn.awesem.com/images/120x60.png"></a>
					</div>
					<div class="widget aw_ad120x60_widget" id="aw_ad120x60_widget-3">
						<a href="http://www.awesemthemes.com"><img alt="" src="http://cdn.awesem.com/images/120x60.png"></a>
					</div>
				</div><!-- END .grid-2 
				<!-- BEGIN .grid-2 --
				<div class="grid-2">
					<div class="widget aw_ad120x240_widget" id="aw_ad120x240_widget-2">
						<a href="http://www.awesemthemes.com"><img alt="" src="http://cdn.awesem.com/images/120x240.png"></a>
					</div>
				</div><!-- END .grid-2 --
			-->
				<div class="clear"></div>
			</div><!-- END .container -->
		</div><!-- END #sidebar -->
	</body>
</html>