	@if($noticias)
	<!-- BEGIN .slider-wrap -->
		<div class="slider-wrap .wrap-aw_sliderblog_widget-2" style="opacity: 1;">
	
			<!-- BEGIN #slider -->
			<div class="flexslider" id="slider-aw_sliderblog_widget-2">
				
				<!-- BEGIN .slides -->
				
				<!-- END .slides -->
								
			<div class="flex-viewport" style="overflow: hidden; position: relative;">
				<ul class="slides" style="width: 1200%; -moz-transition-duration: 0.6s; -moz-transform: translate3d(-1200px, 0px, 0px);">
					
					@foreach($noticias as $noticia)
					<li class="slider-item clone" style="width: 600px; float: left; display: block;">
						
												
						<a title="{{ $noticia->post_title }}" href="{{ URL::to_action('post::post',array($noticia->id)) }}">
							@if($noticia->featuredimage)
							<img alt="{{ $noticia->post_title }}" class="attachment-slider-image-blog wp-post-image" src="{{ $noticia->featuredimage->url }}">
							@endif
						</a>
						
												
						<div class="slider-caption" style="display: none; opacity: 1;">
							<div class="slider-caption-wrap">
								<p class="slider-caption-title">{{ $noticia->post_title }}</p>
								<?php
								preg_match("#<p[^>]*>(.*)</p>#isU",$noticia->post_body,$matches);		
								
								//Log::write('matches',$matches);						
								?>
								<p>{{ strip_tags($matches[0]) }}</p>
								<p class="slider-caption-link"><a title="{{ $noticia->post_title }}" href="{{ URL::to_action('post::post',array($noticia->id)) }}">Leer el resto de esta publicacion</a></p>
							</div>
						</div>
							
					</li>
	
					@endforeach
				</ul>
			</div>

		</div>
			<!-- END #slider -->
		
		</div>
		<!-- END .slider-wrap -->
	@endif