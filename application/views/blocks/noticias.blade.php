<!-- BEGIN .grid-4 -->
			<div class="grid-4">
			
				<h3 class="widget-title">Ultimas Noticias</h3>

				@if($noticias)
					<!-- BEGIN .the-latest -->
					<div class="the-latest">
					
											
						<!-- BEGIN .post-thumb -->
						<div class="post-thumb">
								
							<a href="{{ URL::to_action("post::post",array($noticias[0]->id)) }}" title="{{ $noticias[0]->post_title }}">
								@if($noticias[0]->featuredimage)
									<img src="{{ $noticias[0]->featuredimage->get_version("730x365")->url }}" class="attachment-slider-image-blog wp-post-image" alt="{{ $noticias[0]->post_title }}" />
								@endif
							</a>
								
						</div>
						<!-- END .post-thumb -->
						
											
					</div>
					<!-- END .the-latest -->

					<!-- POR CADA POST -->
					@foreach($noticias as $noticia)

						<!-- BEGIN .floated-thumb -->
						<div class="floated-thumb">
										
													
							<!-- BEGIN .post-thumb -->
							<div class="post-thumb">

								<a href="{{ URL::to_action("post::post",array($noticia->id)) }}" title="{{ $noticia->post_title }}a">
								@if($noticia->featuredimage)
									<img src="{{ $noticia->featuredimage->get_version("50x50")->url }}" class="attachment-grid-1 wp-post-image" alt="{{ $noticia->post_title }}" />
								@endif
								</a>
								
							</div>
							<!-- END .post-thumb -->
							
												
							<!-- BEGIN .post-meta -->
							<div class="post-meta">
							
								<p><a class="meta-title" href="{{ URL::to_action("post::post",array($noticia->id)) }}" title="{{ $noticia->post_title }}">{{ $noticia->post_title }}</a><br />{{ strftime(' %a %d, %Y', strtotime($noticia->created_at)) }}&middot; 
									<!--<a href="{{ URL::to_action("post::post",array($noticia->id)) }}#comments" rel="nofollow" title="Comente en {{ $noticia->post_title }}">Sin comentarios</a>-->
								</p>
									
							</div>
							<!-- END .post-meta -->
							
							<div class="clear"></div>
										
						</div>
						<!-- END .floated-thumb -->
					@endforeach				

					<!-- FIN POR CADA POST -->
				@else
					Todavia no hay noticias publicadas
				@endif
			</div>
			<!-- END .grid-4 -->