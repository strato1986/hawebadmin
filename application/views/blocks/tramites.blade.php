<!-- BEGIN .grid-4 -->
			<div class="grid-4">
			
				<h3 class="widget-title">Guia de Tramites</h3>

				<!-- POR CADA POST -->
				@forelse($tramites as $tramite)

						<!-- BEGIN .floated-thumb -->
						<div class="floated-thumb">
										
													
							<!-- BEGIN .post-thumb -->
							<div class="post-thumb">

								<a href="{{ asset($tramite->url) }}" title="{{ $tramite->document_title }}a">
									<img src="{{ asset('/img/tramites.jpg') }}" class="attachment-grid-1 wp-post-image" alt="{{ $tramite->post_title }}" />
								</a>
								
							</div>
							<!-- END .post-thumb -->
							
												
							<!-- BEGIN .post-meta -->
							<div class="post-meta">
							
								<p><a class="meta-title" href="{{ asset($tramite->url) }}" title="{{ $tramite->document_title }}">{{ $tramite->document_title }}</a><br />{{ strftime(' %a %d, %Y', strtotime($tramite->created_at)) }}&middot; 
									<!--<a href="{{ URL::to_action("post::post",array($tramite->id)) }}#comments" rel="nofollow" title="Comente en {{ $tramite->document_title }}">Sin comentarios</a>-->
								</p>
									
							</div>
							<!-- END .post-meta -->
							
							<div class="clear"></div>
										
						</div>
						<!-- END .floated-thumb -->

					<!-- FIN POR CADA POST -->
				@empty
					Todavia no hay publicaciones
				@endforelse
			</div>
			<!-- END .grid-4 -->