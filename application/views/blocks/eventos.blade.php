<div class="grid-4">
			
				<h3 class="widget-title">Eventos</h3>
				
								
				@if($posts_eventos)				
					<!-- BEGIN .the-latest -->
					<div class="the-latest">
					
											
						<!-- BEGIN .post-thumb -->
						<div class="post-thumb">
								
							<a title="{{ $posts_eventos[0]->post_title }}" href="{{ URL::to_action("post::post",array($posts_eventos[0]->id)) }}">
								<img alt="nice" class="attachment-slider-image-blog wp-post-image" src="{{ $posts_eventos[0]->featuredimage->get_version("730x365")->url }}">
							</a>
								
						</div>
						<!-- END .post-thumb -->
						
											
					</div>
					<!-- END .the-latest -->
					
					<!-- POR CADA POST -->
					@foreach($posts_eventos as $evento)				
						<!-- BEGIN .floated-thumb -->
						<div class="floated-thumb">
										
													
							<!-- BEGIN .post-thumb -->
							<div class="post-thumb">
								
								<a title="{{ $evento->post_title }}" href="{{ URL::to_action("post::post",array($evento->id)) }}"><img alt="nice" class="attachment-grid-1 wp-post-image" src="{{ $evento->featuredimage->get_version("50x50")->url }} "></a>
								
							</div>
							<!-- END .post-thumb -->
							
												
							<!-- BEGIN .post-meta -->
							<div class="post-meta">
							
								<p><a title="{{ $evento->post_title }}" href="{{ URL::to_action("post::post",array($evento->id)) }}" class="meta-title">{{ $evento->post_title }}</a><br>{{ strftime(' %a %d, %Y', strtotime($evento->created_at)) }} · 
									<!--<a title="Comente en {{ $evento->post_title }}" rel="nofollow" href="{{ URL::to_action("post::post",array($evento->id)) }}#comments">Sin Comentarios</a></p>-->
									
							</div>
							<!-- END .post-meta -->
							
							<div class="clear"></div>
										
						</div>
						<!-- END .floated-thumb -->
					@endforeach														
					
			@else
				No hay publicaciones de evento
			@endif																
</div>