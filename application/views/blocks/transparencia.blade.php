<!-- BEGIN .grid-4 -->
			<div class="grid-4">
			
				<h3 class="widget-title">Transparencia</h3>

				<!-- POR CADA POST -->
				@forelse($transparencias as $transparencia)

						<!-- BEGIN .floated-thumb -->
						<div class="floated-thumb">
										
													
							<!-- BEGIN .post-thumb -->
							<div class="post-thumb">

								<a href="{{ asset($transparencia->url) }}" title="{{ $transparencia->document_title }}a">
									<img src="{{ asset('/img/ico_pdf.jpg') }}" class="attachment-grid-1 wp-post-image" alt="{{ $transparencia->post_title }}" />
								</a>
								
							</div>
							<!-- END .post-thumb -->
							
												
							<!-- BEGIN .post-meta -->
							<div class="post-meta">
							
								<p><a class="meta-title" href="{{ asset($transparencia->url) }}" title="{{ $transparencia->document_title }}">{{ $transparencia->document_title }}</a><br />{{ strftime(' %a %d, %Y', strtotime($transparencia->created_at)) }}&middot; 
									<!--<a href="{{ URL::to_action("post::post",array($transparencia->id)) }}#comments" rel="nofollow" title="Comente en {{ $transparencia->document_title }}">Sin comentarios</a>-->
								</p>
									
							</div>
							<!-- END .post-meta -->
							
							<div class="clear"></div>
										
						</div>
						<!-- END .floated-thumb -->

					<!-- FIN POR CADA POST -->
				@empty
					Todavia no hay publicaciones
				@endforelse
			</div>
			<!-- END .grid-4 -->